/* jslint node:true */
/* global describe:true */
/* global before:true */
/* global it:true */

'use strict';

var expect = require('expect.js'),
    manifestFormat = require('../manifestformat.js'),
    safe = require('safetydance'),
    _ = require('underscore');

describe('parseManifest', function () {
    it('errors for empty string', function () {
        expect(manifestFormat.parseString('').error).to.be.an(Error);
    });

    it('errors for invalid json', function () {
        expect(manifestFormat.parseString('garbage').error).to.be.an(Error);
    });

    var manifest = {
        id: 'io.cloudron.test',
        title: 'Bar App',
        description: 'Long long ago, there was foo',
        tagline: 'Not your usual Foo App',
        author: 'Herbert Burgermeister',
        manifestVersion: 1,
        version: '0.1.2',
        dockerImage: 'girish/foo:0.2',
        healthCheckPath: '/',
        httpPort: 23,
        website: 'https://example.com',
        contactEmail: 'support@example.com'
    };

    manifestFormat.SCHEMA_V1.required.forEach(function (key) {
        var manifestCopy = _.extend({ }, manifest);
        delete manifestCopy[key];
        it('errors for missing ' + key, function () {
            expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
        });
    });

    new Array(null, [ 23 ], [ "mysql", 34 ], [ null, "mysql" ]).forEach(function (invalidAddon, idx) {
        it('fails for invalid addon testcase ' + idx, function () {
            var manifestCopy = _.extend({ }, manifest);
            manifestCopy.addons = invalidAddon;
            expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
        });
    });

    it('fails for bad version', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.version = '0.2';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad id', function () {
        var manifestCopy = _.extend({ }, manifest);
        ['simply', '12de', 'in..x', 'x.com', 'no.hy-phen' ].forEach(function(badId) {
            manifestCopy.id = badId;
            expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
        });
    });

    it('fails for bad minBoxVersion', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.minBoxVersion = '0.2';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad maxBoxVersion', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.maxBoxVersion = '0.2';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad targetBoxVersion', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.targetBoxVersion = '0.2';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad manifestVersion', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.manifestVersion = 2;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad tcpPorts', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.tcpPorts = 45;

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad capabilities', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.capabilities = [ 'test' ];

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad changelog', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.changelog = 34;

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.changelog = [ ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.changelog = [ null ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.changelog = [ 56 ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad tcpPorts', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.tcpPorts = 45;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.tcpPorts = { "env$": { } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad env

        manifestCopy.tcpPorts = { "env": { } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // missing description

        manifestCopy.tcpPorts = { "env": { description: 34 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad description

        manifestCopy.tcpPorts = { "env": { title: 34 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad title

        manifestCopy.tcpPorts = { "env": { title: "t", description: "long enough" } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // short title

        manifestCopy.tcpPorts = { "env": { description: "description", containerPort: "invalid" } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad containerPort

        manifestCopy.tcpPorts = { "env": { description: "description", containerPort: NaN } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad containerPort

        manifestCopy.tcpPorts = { "env": { description: "description", containerPort: -2 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad containerPort

        manifestCopy.tcpPorts = { "env": { description: "description", defaultValue: "invalid" } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad defaultValue

        manifestCopy.tcpPorts = { "env": { description: "description", defaultValue: NaN } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad defaultValue

        manifestCopy.tcpPorts = { "env": { description: "description", defaultValue: -2 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad defaultValue

        manifestCopy.tcpPorts = { "env": { description: "description", defaultValue: 1000 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error); // bad defaultValue
    });

    it('fails for bad addon', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.addons = { 'andy': { } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad tags', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.tags = [ 234 ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy = _.extend({ }, manifest);
        manifestCopy.tags = [ 'again', 'and', 'again' ]; // duplicate
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for bad scheduler addon', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.addons = {
            scheduler: {
                'nice': 'try'
            }
        };

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);

        manifestCopy.addons = {
            scheduler: {
                'task1': { schedule: '1 2 3 4 m', command: 'blah' }
            }
        };

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('succeeds for good scheduler addon', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.addons = {
            'scheduler': {
                'task1': { schedule: '* * * * *', command: 'blah' }
            }
        };

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds for good capabilities', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.capabilities = [ ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);

        manifestCopy = _.extend({ }, manifest);
        manifestCopy.capabilities = [ 'net_admin' ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds for good tcpPorts', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.tcpPorts = { "env": { title: "12345", description: "12345", containerPort: 546 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);

        manifestCopy = _.extend({ }, manifest);
        manifestCopy.tcpPorts = { "env": { title: "12345", description: "12345", containerPort: 65535 } };
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds for minimal valid manifest', function () {
        expect(manifestFormat.parseString(JSON.stringify(manifest)).manifest).to.eql(manifest);
    });

    it('succeeds for maximal valid manifest', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.minBoxVersion = '0.0.1';
        manifestCopy.maxBoxVersion = '1.0.0';
        manifestCopy.targetBoxVersion = '1.0.0';
        manifestCopy.addons = {
            'mysql': { },
            'postgresql': { },
            'mongodb': { cacheSize: 34 }
        };
        manifestCopy.changelog = 'Change upload size limit';
        manifestCopy.tags = [ 'wiki', 'collaboration' ];
        manifestCopy.capabilities = [ 'net_admin' ];

        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('fails for bad memoryLimit', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.memoryLimit = '1p';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('succeeds for int memoryLimit', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.memoryLimit = 123456;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds for string memoryLimit', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.memoryLimit = '10MB';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });


    it('fails for non uri type mediaLinks', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.mediaLinks = [ 'http://foobar.de', 'http://baz', 'foobar' ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('fails for duplicate mediaLinks', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.mediaLinks = [ 'http://foobar.de', 'http://baz.com', 'http://foobar.de' ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).error).to.be.an(Error);
    });

    it('succeeds with mediaLinks', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.mediaLinks = [ 'http://foobar.de', 'http://baz.com' ];
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds with postInstallMessage', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.postInstallMessage = 'Can you see this? Thanks for installing';
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds with customAuth', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.customAuth = false;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });

    it('succeeds with optionalSso', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.optionalSso = true;
        expect(manifestFormat.parseString(JSON.stringify(manifestCopy)).manifest).to.eql(manifestCopy);
    });
});

describe('isId', function () {
    it('return false for invalid ids', function () {
        expect(manifestFormat.isId('combarzoo')).to.be(false);
        expect(manifestFormat.isId('com.bar-zoo')).to.be(false);
        expect(manifestFormat.isId('c.bar.zoo')).to.be(false);
    });

    it('succeeds for valid ids', function () {
        expect(manifestFormat.isId('com.bar.zoo')).to.be(true);
        expect(manifestFormat.isId('com.bar_zoo')).to.be(true);
        expect(manifestFormat.isId('com.barzoo')).to.be(true);
        expect(manifestFormat.isId('co.bar.zoo')).to.be(true);
    });
});

describe('checkAppstoreRequirements', function () {
    var manifest = {
        id: 'io.cloudron.test',
        title: 'Bar App',
        description: 'Long long ago, there was foo',
        tagline: 'Not your usual Foo App',
        author: 'Herbert Burgermeister',
        manifestVersion: 1,
        version: '0.1.2',
        dockerImage: 'girish/foo:0.2',
        healthCheckPath: '/',
        httpPort: 23,
        website: 'https://example.com',
        contactEmail: 'support@example.com',
        tags: [ 'something' ],
        mediaLinks: [ 'https://foo.ong' ],
        icon: 'file://icon.png',
        changelog: 'file://changelog.md',
        customAuth: false,
        postInstallMessage: 'Nice!'
    };

    it('fails without tags', function () {
        var manifestCopy = _.extend({ }, manifest);
        delete manifestCopy.tags;
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
    });

    it('fails without mediaLinks', function () {
        var manifestCopy = _.extend({ }, manifest);
        delete manifestCopy.mediaLinks;
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
    });

    it('fails without icon', function () {
        var manifestCopy = _.extend({ }, manifest);
        delete manifestCopy.icon;
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
    });

    it('fails without changelog', function () {
        var manifestCopy = _.extend({ }, manifest);
        delete manifestCopy.changelog;
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
    });

    it('fails with prerelease version', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.version = '1.2.3-pre';
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
    });

    it('fails with build version', function () {
        var manifestCopy = _.extend({ }, manifest);
        manifestCopy.version = '1.2.3+build';
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be.an(Error);
    });

    it('succeeds with all requirements', function () {
        var manifestCopy = _.extend({ }, manifest);
        expect(manifestFormat.checkAppstoreRequirements(manifestCopy)).to.be(null);
    });
});

