'use strict';

var assert = require('assert'),
    CronJob = require('cron').CronJob,
    packageNameRe = require('java-packagename-regex'),
    safe = require('safetydance'),
    semver = require('semver'),
    tv4 = require('tv4').freshApi(),
    validator = require('validator');

var SCHEMA_V1 = {
    type: 'object',
    properties: {
        'addons': {
            type: 'object',
            patternProperties: {
                '^(ldap|redis|sendmail|oauth|mysql|postgresql|mongodb|localstorage|email|recvmail|tls)$': {
                    type: 'object',
                    additionalProperties: true
                }
            },
            properties: {
                'scheduler': {
                    type: 'object',
                    patternProperties: {
                        '^[a-zA-Z0-9_]+$': {
                            type: 'object',
                            properties: {
                                'schedule': {
                                    type: 'string',
                                    format: 'cronpattern'
                                },
                                'command': {
                                    type: 'string'
                                }
                            }
                        }
                    }
                }
            }
        },
        'author': {
            type: 'string',
            minLength: 2
        },
        'changelog': {
            type: 'string',
            minLength: 5
        },
        'configurePath': {
            type: 'string',
            minLength: 1
        },
        'contactEmail': {
            type: 'string',
            format: 'email'
        },
        'customAuth': {
            type: 'boolean',
        },
        'description': {
            type: 'string',
            minLength: 5
        },
        'dockerImage': {
            type: 'string',
            minLength: 1
        },
        'healthCheckPath': {
            type: 'string',
            minLength: 1
        },
        'httpPort': {
            type: 'integer',
            minimum: 1,
            maximum: 65535
        },
        'icon': {
            type: 'string'
        },
        'id': {
            type: 'string',
            format: 'reverseDomain'
        },
        'manifestVersion': {
            type: 'integer',
            minimum: 1,
            maximum: 1
        },
        'maxBoxVersion': {
            type: 'string',
            format: 'semver'
        },
        'mediaLinks': {
            type: 'array',
            uniqueItems: true,
            items: {
                type: 'string',
                format: 'uri'
            }
        },
        'memoryLimit': {
            oneOf: [{
                type: 'integer',
                minimum: 1
            }, {
                type: 'string',
                format: 'byteString'
            }]
        },
        'minBoxVersion': {
            type: 'string',
            format: 'semver'
        },
        'optionalSso': {
            type: 'boolean'
        },
        'capabilities': {
            type: 'array',
            items: {
                type: 'string',
                pattern: '^(net_admin)$'
            },
            uniqueItems: true
        },
        'postInstallMessage': {
            type: 'string',
            minLength: 5
        },
        'tagline': {
            type: 'string',
            minLength: 5
        },
        'tags': {
            type: 'array',
            items: {
                type: 'string'
            },
            uniqueItems: true
        },
        'targetBoxVersion': {
            type: 'string',
            format: 'semver'
        },
        'tcpPorts': {
            type: 'object',
            patternProperties: {
                '^[a-zA-Z0-9_]+$': {
                    type: 'object',
                    properties: {
                        'title': {
                            type: 'string',
                            minLength: 2
                        },
                        'description': {
                            type: 'string',
                            minLength: 5
                        },
                        'containerPort': {
                            type: 'integer',
                            minimum: 1,
                            maximum: 65535
                        },
                        'defaultValue': {
                            type: 'integer',
                            minimum: 1024,
                            maximum: 65535
                        }
                    },
                    required: [ 'title', 'description' ]
                }
            }
        },
        'title': {
            type: 'string',
            minLength: 2
        },
        'version': {
            type: 'string',
            format: 'semver'
        },
        'website': {
            type: 'string',
            format: 'uri'
        }
    },
    required: [ 'id', 'manifestVersion', 'version', 'healthCheckPath', 'httpPort', 'title', 'description', 'tagline', 'website', 'contactEmail', 'author' ]
};

exports = module.exports = {
    isId: isId,
    parse: parse,
    parseString: parseString,
    parseFile: parseFile,
    checkAppstoreRequirements: checkAppstoreRequirements,

    SCHEMA_V1: SCHEMA_V1
};

function isId(id) {
    return packageNameRe().test(id);
}

tv4.addFormat('semver', function (data, schema) {
    return semver.valid(data) ? null : 'not a semver';
});

tv4.addFormat('uri', function (data, schema) {
    var options = {
        protocols: [ 'http', 'https' ],
        require_tld: true,
        require_protocol: false,
        allow_underscores: false,
        host_whitelist: false,
        host_blacklist: false
    };

    if (!validator.isURL(data, options)) return 'Invalid URL';

    return null;
});

tv4.addFormat('reverseDomain', function (data, schema) {
    return isId(data) ? null : 'Invalid id';
});

tv4.addFormat('email', function (data, schema) {
    return validator.isEmail(data) ? null : 'Invalid email';
});

tv4.addFormat('cronpattern', function (data, schema) {
    if (data.split(' ').length !== 5) return 'Invalid cron pattern (note: seconds disallowed)'; // second is disallowed

    try {
        new CronJob('00 ' + data, function() { // second is disallowed
        });
        return null;
    } catch(ex) {
        return 'Invalid cron pattern';
    }
});

tv4.addFormat('byteString', function (data, schema) {
    var results = data.match(/^((-|\+)?(\d+(?:\.\d+)?)) *(kb|mb|gb|tb)$/i);
    return !results ? 'Invalid size' : null;
});

function parse(manifest) {
    assert(manifest && typeof manifest === 'object');

    var result = tv4.validateResult(manifest, SCHEMA_V1, false /* recursive */, true /* banUnknownProperties */);
    if (!result.valid) return new Error(result.error.message + (result.error.dataPath ? ' @ ' + result.error.dataPath : ''));

    return null;
}

function parseString(manifestJson) {
    assert(typeof manifestJson === 'string');

    var manifest = safe.JSON.parse(manifestJson);

    var error = manifest ? parse(manifest) : new Error('Unable to parse manifest: ' + safe.error.message);

    return { manifest: error ? null : manifest, error: error };
}

function parseFile(manifestFile) {
    assert(typeof manifestFile === 'string');

    var manifestJson = safe.fs.readFileSync(manifestFile, 'utf8');
    if (!manifestJson) return { manifest: null, error: new Error('Unable to read file: ' + safe.error.message) };

    return parseString(manifestJson);
}

// tags which are required by appstore (but not required for installation)
function checkAppstoreRequirements(manifest) {
    assert(typeof manifest === 'object');

    if (!('tags' in manifest)) return new Error('tags is missing in manifest');
    if (manifest.tags.length === 0) return new Error('tags is empty in manifest');

    if (!('mediaLinks' in manifest)) return new Error('mediaLinks is missing in manifest');
    if (manifest.mediaLinks.length === 0) return new Error('mediaLinks is empty in manifest');

    if (!('icon' in manifest)) return new Error('icon is missing in manifest');
    if (manifest.icon === '') return new Error('icon is empty in manifest');

    if (!('changelog' in manifest)) return new Error('changelog is missing in manifest');
    if (manifest.changelog === '') return new Error('changelog is empty in manifest');

    if (manifest.developmentMode) return new Error('developmentMode must be set to false');

    if (manifest.addons && Object.keys(manifest.addons).some(function (addon) { return /^_.*$/.test(addon); })) {
        return new Error('cannot publish with reserved addons');
    }

    var p = semver.parse(manifest.version);
    if (p.prerelease.length !== 0) return new Error('Version cannot contain prerelease');
    if (p.build.length !== 0) return new Error('Build cannot contains build');

    return null;
}

